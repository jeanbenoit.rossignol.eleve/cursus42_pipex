/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/29 09:31:36 by jrossign          #+#    #+#             */
/*   Updated: 2022/06/10 16:41:58 by jean-beno        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PIPEX_H
# define PIPEX_H

# include <stdio.h>
# include <unistd.h>
# include <fcntl.h>
# include <stdlib.h>
# include <string.h>
# include <errno.h>
# include "../libft/include/libft.h"

# define CMD_NOT_FOUND "command not found: "

typedef struct s_object
{
	int		fds[2];
	int		argc;
	int		first_cmd;
	int		first_file;
	char	**envp;
	char	**cmds;
	char	**path;
	char	**cmds_path;
}			t_object;

//check_args.c
void	check_files(t_object *obj, char **argv);
void	check_cmds(t_object *obj, char **argv);

//env_path_getter.c
void	env_access(t_object *obj);
int		try_access(t_object *obj, char **cmd, int index);
void	access_cmd(t_object *obj, char *first_cmd, int index);

//fork_utils.c
void	start_fork(t_object *obj);
void	execute_first_cmd(t_object *obj, char **cmds_split, int pipe_fd[2]);
void	execute_other_cmd(t_object *obj, char **cmds_split, int pipe_fd[2]);

//free_struct.c
void	free_struct(t_object *obj);
void	free_split(char **freeing);
void	close_fd(t_object *obj);

//object_setter.c
void	set_object(t_object *obj, int argc, char **argv, char **envp);
char	**get_path(char **envp);

//pipex.c
int		main(int argc, char **argv, char **envp);
void	print_obj(t_object *obj);

//print_error.c
void	print_error(char *error_message, t_object *obj);
void	print_strerror(int error, char *file_name, t_object *obj, int i);
void	print_strerror_outfile(int error, char *file_name, t_object *obj);
void	print_cmd_not_found(char **cmd, t_object *obj, int i);

#endif
