# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/04/29 11:31:13 by jrossign          #+#    #+#              #
#    Updated: 2022/06/20 14:29:23 by jrossign         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#--Project name--#
PROJECT_NAME	= pipex

#--C FILES--# > all .c files
C_FILES			= $(notdir $(wildcard src/*.c))

#--C FILES TO O FILES--# > where you make the .o files dependencies
O_FILES			= $(C_FILES:.c=.o)

#--NAME--# > name of the project
NAME			= $(PROJECT_NAME)

#--COMMANDS--# > all the bash command you will use in the Makefile
GCC				= gcc
MKDIR			= mkdir
MAKE			= make
RM				= rm
NORMINETTE		= norminette
SUB_INIT		= git submodule init
SUB_UPDATE		= git submodule update

#--FLAGS--# > flags used by the command above
ERROR_FLAGS		= -Werror -Wextra -Wall
P_FLAG			= -p
O_FLAG			= -o
C_FLAG			= -c
RF_FLAG			= -rf
F_FLAG			= -f
MAKE_C_FLAG		= -C
LIB_FLAG		= -L./libft -lft

#--PATH--# > path to the files
SRC_DIR			= src/
OBJ_DIR			= obj/
INC_DIR			= include/
LIBFT_DIR		= libft/

#--VPATH--#
VPATH			= $(SRC_DIR)

#--PREFIX--#
PRE_SRC		= $(addprefix $(SRC_DIR), $(C_FILES))
PRE_OBJ		= $(addprefix $(OBJ_DIR), $(O_FILES))

#--ACTIONS--# > all the thing you want your Makefile to do
$(OBJ_DIR)%.o:		%.c
				@$(MKDIR) $(P_FLAG) $(OBJ_DIR)
				@$(GCC) $(ERROR_FLAGS) $(O_FLAG) $@ $(C_FLAG) $<

$(NAME):			$(PRE_OBJ)
				@echo "Compiling $(PROJECT_NAME)..."
				@$(MAKE) $(MAKE_C_FLAG) $(LIBFT_DIR)
				@$(GCC) $(ERROR_FLAGS) $(PRE_OBJ) $(O_FLAG) $(NAME) $(LIB_FLAG)
				@echo "Compiling $(PROJECT_NAME) done."

submodule:
				@$(SUB_INIT)
				@$(SUB_UPDATE)

norminette:
				@$(NORMINETTE)

all:				$(NAME)

clean: 
				@echo "Removing $(PROJECT_NAME) object files..."
				@$(MAKE) $(MAKE_C_FLAG) $(LIBFT_DIR) clean
				@$(RM) $(F_FLAG) $(PRE_OBJ)
				@$(RM) $(RF_FLAG) $(OBJ_DIR)
				@echo "Removing $(PROJECT_NAME) object files done."

fclean:				clean
				@echo "Removing $(PROJECT_NAME) program..."
				@$(MAKE) $(MAKE_C_FLAG) $(LIBFT_DIR) fclean
				@$(RM) $(F_FLAG) $(NAME)
				@echo "Removing $(PROJECT_NAME) program done."

re:					fclean all

.PHONY: all clean fclean re norminette submodule
