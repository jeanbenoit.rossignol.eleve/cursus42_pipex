/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_path_getter.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/03 12:24:31 by jrossign          #+#    #+#             */
/*   Updated: 2022/06/16 16:36:04 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pipex.h"

void	env_access(t_object *obj)
{
	int		i;
	int		exit_code;
	char	**splitted_cmd;

	i = 0;
	while (obj->cmds[i])
	{
		splitted_cmd = ft_split(obj->cmds[i], ' ');
		if (ft_strncmp(splitted_cmd[0], "exit", 5) == 0)
		{
			exit_code = ft_atoi(splitted_cmd[1]);
			free_split(splitted_cmd);
			free_struct(obj);
			exit(exit_code);
		}
		if (!try_access(obj, splitted_cmd, i))
			print_cmd_not_found(splitted_cmd, obj, i);
		free_split(splitted_cmd);
		i++;
	}
}

int	try_access(t_object *obj, char **cmd, int index)
{
	char	*first_cmd;

	first_cmd = ft_strjoin("/", cmd[0]);
	if (access(cmd[0], X_OK) == 0)
		obj->cmds_path[index] = ft_strdup(cmd[0]);
	access_cmd(obj, first_cmd, index);
	free(first_cmd);
	if (obj->cmds_path[index] == NULL)
		return (0);
	return (1);
}

void	access_cmd(t_object *obj, char *first_cmd, int index)
{
	int		i;
	char	*access_test;

	i = 0;
	while (obj->path && obj->path[i])
	{
		access_test = ft_strjoin(obj->path[i], first_cmd);
		if (access(access_test, X_OK) == 0)
		{
			obj->cmds_path[index] = access_test;
			break ;
		}
		free(access_test);
		i++;
	}
}
