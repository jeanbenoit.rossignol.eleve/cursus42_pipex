/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_args.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/01 13:38:32 by jrossign          #+#    #+#             */
/*   Updated: 2022/06/10 13:32:49 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pipex.h"

void	check_files(t_object *obj, char **argv)
{
	if (obj->argc != 5)
		print_error("Argc is not 5", obj);
	obj->fds[1] = open(argv[obj->argc - 1], O_RDWR | O_CREAT | O_TRUNC, 0666);
	if (access(argv[1], R_OK) != 0)
		print_strerror(errno, argv[1], obj, 0);
	else
		obj->fds[0] = open(argv[1], O_RDONLY);
	if (obj->fds[1] == -1)
		print_strerror_outfile(errno, argv[4], obj);
}

void	check_cmds(t_object *obj, char **argv)
{
	int		cmd_amount;
	int		i;

	cmd_amount = obj->argc - 3;
	i = 0;
	obj->cmds = ft_calloc(sizeof(char *), cmd_amount + 1);
	obj->cmds_path = ft_calloc(sizeof(char *), cmd_amount + 1);
	while (i < cmd_amount)
	{
		obj->cmds[i] = argv[i + 2];
		i++;
	}
}
