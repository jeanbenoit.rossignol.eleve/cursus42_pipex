/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fork_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/01 16:14:25 by jrossign          #+#    #+#             */
/*   Updated: 2022/06/13 14:16:42 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pipex.h"

void	start_fork(t_object *obj)
{
	char	**cmd_split;
	int		pid;
	int		pipe_fd[2];

	cmd_split = ft_split(obj->cmds[0], ' ');
	pipe(pipe_fd);
	pid = fork();
	if (pid == 0)
		execute_first_cmd(obj, cmd_split, pipe_fd);
	free_split(cmd_split);
	cmd_split = ft_split(obj->cmds[1], ' ');
	pid = fork();
	if (pid == 0)
		execute_other_cmd(obj, cmd_split, pipe_fd);
	free_split(cmd_split);
	close(pipe_fd[0]);
	close(pipe_fd[1]);
}

void	execute_first_cmd(t_object *obj, char **cmd_split, int pipe_fd[2])
{
	if (obj->first_file == 0)
	{
		dup2(obj->fds[0], 0);
		close(obj->fds[0]);
		dup2(pipe_fd[1], 1);
		close(pipe_fd[1]);
		close(pipe_fd[0]);
		if (obj->first_cmd == 0)
			execve(obj->cmds_path[0], cmd_split, obj->envp);
	}
	close(pipe_fd[1]);
	close(pipe_fd[0]);
	close(obj->fds[1]);
	free_struct(obj);
	free_split(cmd_split);
	exit(0);
}

void	execute_other_cmd(t_object *obj, char **cmd_split, int pipe_fd[2])
{
	dup2(pipe_fd[0], 0);
	close(pipe_fd[0]);
	dup2(obj->fds[1], 1);
	close(obj->fds[1]);
	close(pipe_fd[1]);
	execve(obj->cmds_path[1], cmd_split, obj->envp);
	free_struct(obj);
	free_split(cmd_split);
	exit(0);
}
