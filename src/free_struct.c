/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free_struct.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/01 12:05:11 by jrossign          #+#    #+#             */
/*   Updated: 2022/06/13 14:05:40 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pipex.h"

void	free_struct(t_object *obj)
{
	if (obj->first_cmd == 1)
		free(obj->cmds_path[1]);
	free_split(obj->cmds_path);
	if (obj->path)
		free_split(obj->path);
	free(obj->cmds);
	free(obj);
}

void	free_split(char **freeing)
{
	int	i;

	i = 0;
	while (freeing[i])
	{
		free(freeing[i]);
		i++;
	}
	free(freeing);
}

void	close_fd(t_object *obj)
{
	if (obj->first_file == 0)
		close(obj->fds[0]);
	close(obj->fds[1]);
}
