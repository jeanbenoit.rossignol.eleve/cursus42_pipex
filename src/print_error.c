/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_error.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/01 13:47:49 by jrossign          #+#    #+#             */
/*   Updated: 2022/06/16 14:52:31 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pipex.h"

//write on stderr
void	print_error(char *error_message, t_object *obj)
{
	write(2, error_message, ft_strlen(error_message));
	free_split(obj->path);
	free(obj);
	exit(1);
}

//write on stderr
void	print_strerror(int error, char *file_name, t_object *obj, int i)
{
	if (i == 0)
	{
		obj->first_file = 1;
		write(2, strerror(error), ft_strlen(strerror(error)));
		write(2, ": ", 2);
		write(2, file_name, ft_strlen(file_name));
		write(2, "\n", 1);
	}
	else
	{
		write(2, strerror(error), ft_strlen(strerror(error)));
		write(2, ": ", 2);
		write(2, file_name, ft_strlen(file_name));
		write(2, "\n", 1);
		free_split(obj->path);
		free(obj);
		exit(0);
	}
}

void	print_strerror_outfile(int error, char *file_name, t_object *obj)
{
	write(2, strerror(error), ft_strlen(strerror(error)));
	write(2, ": ", 2);
	write(2, file_name, ft_strlen(file_name));
	free_split(obj->path);
	free(obj);
	exit(1);
}

void	print_cmd_not_found(char **cmd, t_object *obj, int i)
{
	char	*error;

	if (i == 0)
	{
		error = ft_strjoin(CMD_NOT_FOUND, cmd[0]);
		obj->first_cmd = 1;
		write(2, error, ft_strlen(error));
		write(2, "\n", 1);
		free(error);
	}
	else
	{
		close_fd(obj);
		error = ft_strjoin(CMD_NOT_FOUND, cmd[0]);
		write(2, error, ft_strlen(error));
		write(2, "\n", 1);
		free(error);
		free_split(cmd);
		free_struct(obj);
		exit(127);
	}
}
