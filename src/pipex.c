/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pipex.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/29 09:27:52 by jrossign          #+#    #+#             */
/*   Updated: 2022/06/13 14:09:10 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pipex.h"

int	main(int argc, char **argv, char **envp)
{
	t_object	*obj;

	obj = malloc(sizeof(t_object) * 1);
	set_object(obj, argc, argv, envp);
	env_access(obj);
	start_fork(obj);
	close_fd(obj);
	free_struct(obj);
	return (EXIT_SUCCESS);
}
