/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   object_setter.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.c      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/05/01 15:36:36 by jrossign          #+#    #+#             */
/*   Updated: 2022/06/13 14:02:13 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/pipex.h"

void	set_object(t_object *obj, int argc, char **argv, char **envp)
{
	char	**path;

	path = get_path(envp);
	obj->first_cmd = 0;
	obj->first_file = 0;
	obj->argc = argc;
	obj->path = path;
	obj->envp = envp;
	check_files(obj, argv);
	check_cmds(obj, argv);
}

char	**get_path(char **envp)
{
	char	**path_split;
	int		i;

	i = 0;
	while (envp[i] && ft_strncmp(envp[i], "PATH=", 5) != 0)
		i++;
	if (envp[i] == NULL)
		return (NULL);
	path_split = ft_split(envp[i] + 5, ':');
	return (path_split);
}
